a,b,c,d = 1,2,3,4
print(a)
print(c)


def some_func():
    a = 1
    b = {}  # dict()
    v = tuple()
    c = set()
    return a, b, v, c


result = some_func()
print(type(result))

tuple_var = (1, 2, 3, 4)
correct_tuple_var = 1, 2, 3, 4
print(tuple_var)
print(correct_tuple_var)

nested_tuple = (1, 2), (2, 3), (4, 5)
print(nested_tuple)
a = ()
print(a)
print(type(a))
single_item_tuple = 2,  #(2,)
bad_single_item_tuple = (2)

print(type(single_item_tuple))
print(type(bad_single_item_tuple))

a = 1, 2, 3, 4
# some code
a1, a2, a3, a4 = a

def some_func_unpack(a, b, *args, **kwargs):
    print(args)

some_func_unpack(1, 2, 3)
some_func_unpack(1, 2)

a1, b2, *rest = 1,2,3,4,5,6,7,8
print(a1)
print(a2)
print(rest)
rest.append(10)
print(rest)

rest.extend([11])
rest += [12]

new_args = [1,2,3,4]
a,b,c,d,e,f = (*new_args, *[30, 40]) # => (1,2,3,4, *[30,40]) => (1,2,3,4, 30, 40)
print(a, b, e, f)

a,b,c,d,e,f = new_args + [30, 40] # new_args + [30, 40] => [1,2,3,4,30,40]
print(a, b, e, f)
print(new_args)

a1, b2, *rest = 1,2,3,4,5,6,7,8
print(a1, b2, rest)
c3, d4, *rest = rest
print(c3, d4, rest)
a, *args = 1,
print(a)
print(args)

def some_func(a,b,c):
    print(a,b,c)

some_func(*[1,2,3])

a1, b2, *rest = 1,2,3,4,5,6,7,8
new_list = [0, *rest, 100, 200] # IF rest + [100, 200]
print(new_list)

def super_func(a, b, c, d):
    print(a,b,c,d)


super_func(1,2,3,4)
super_func(d=1, c=2, b=3, a=4)
#super_func(1, 2, b=3, d=4)
super_func(1, 2, c=3, d=4)


a,b = (1,2)
kwargs = dict(b=3, d=4)
# super_func(c=3, d=4, 1, 2) => SyntaxError: positional argument follows keyword argument

#super_func(c=3, d=4, 1, 2)

def func_with_default(a, b=2, c=3, d=4):
    print(a,b,c,d)

# func_with_default(a=1, 2, c=4, 5)

func_with_default(10, 20)
kwargs = {'b': 2, 'c':3, 'd': 4}
kwargs['b'] = 20
kwargs['d'] = 40
print(kwargs)
def func_with_default(a, b=2, c=3, d=4, **kwargs):
    print(a,b,c,d)
    print(kwargs)

func_with_default(1,2, c=3, d=5, l=5, g=10)

def func_without_explicit_args(a, *args, l, **kwargs):
    print(args)
    print(kwargs)

func_without_explicit_args(1,2,3, l=1, m=10)

def func_with_explicit_kwargs(a, b, *, l, c, **kwarags):
    print(a)
    print(l)
    print(c)
    print(kwarags)

func_with_explicit_kwargs(1, 2, l=3, c=4, h=10)
# a,b = 1,2

#
def func_with_default(a, /, *, b=2, c=3, d):
    print(a,b,c,d)

func_with_default(1, d=4)

dict_1 = {'a': 1, 'b': 2, 'k': 1}
dict_2 = {'c': 3, 'd': 4, 'k': 10}
print({
    **dict_2,
    **dict_1,
    'd': 10,
    'k': 100,
})

dict(**{'key1': 1}, **{'key2': 2}) # => dict(key1=1, key2=2)
kwargs = {'c':3, 'd':4}
args = (1,)
func_with_default(*args, b=2, **kwargs) # =>func_with_default(1, b=2, c=3, d=4})


def func_with_positional_args(arg1, arg2, *args):
    print(arg1)
    print(arg2)
    print(args)

args = [1,2,3,4,5,6]
func_with_positional_args(*args)
